#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <array>
#include <string>
#include <locale>



#include "DataHelper.hh"

#include "TROOT.h"
#include "TBrowser.h"
#include "Riostream.h"
#include "TH1.h"
#include "TF1.h"
#include "TFile.h"
#include "TRandom.h"
#include "TString.h"
#include "TTree.h"
#include "TBranch.h"
#include "TGraphErrors.h"
#include "TList.h"
#include "TObjArray.h"
#include "TChain.h"
#include "TSystem.h"
#include "TMath.h"
#include "TVector3.h"
#include "TCanvas.h"



bool do_data(string fnames);
bool tranf_data(std::vector<double> &x, std::vector<double> &y, string fname_base);
void Solve(vector<double>& roots, int nest, TF1* f, double s, double e);
void FindRoot(vector<double>& roots, TF1* f,double s, double e, string fname_base);

