#include "analizadados.h"

using namespace std;

template<typename CharT>
class DecimalSeparator : public std::numpunct<CharT>
{
public:
    DecimalSeparator(CharT Separator)
    : m_Separator(Separator)
    {}

protected:
    CharT do_decimal_point()const
    {
        return m_Separator;
    }

private:
    CharT m_Separator;
};

int main() {


	fstream afile;
	afile.open ("archivos.txt", ios::in);
	
	if(afile.is_open())
		cout << "Abrio bien: " << "archivos.txt"  << endl;
	if(afile.fail()) 
		cout << "problemas con el archivo" << endl;
	
	std::vector<string> ar_names;
	ar_names.reserve(20);

	while(true) {      
		if (!afile.good() ) break;
		
		string mpdline, mStemp;
		getline(afile, mpdline);   
		istringstream istr(mpdline.c_str());      
		istr >> mStemp;
		if(mStemp == "" )
		break;
		else	{
//			cout << mStemp;
		ar_names.push_back(mStemp);	
			}	
		} 
		afile.close();

	for(int b=0; b<ar_names.size(); b++){
		if(do_data(ar_names.at(b))==true)
			cout << "Finished ok" << endl;
		else
			cout << "problem with " << ar_names.at(b) << endl;
	}

	



	
	
return 0;
	
}

bool do_data(string fnames){

	cout << "inside funct " << fnames << endl;
	fstream file;
	file.open (fnames, ios::in);
	string fname_base= fnames.substr(0,fnames.size()-4);
	

	int counter=0;
	cout << "begin run " << endl;
	
	if(file.is_open())
		cout << "Abrio bien: " << "file.txt"  << endl;
	if(file.fail()) 
		cout << "problemas con el archivo" << endl;


	std::vector<double> xval;
	std::vector<double> yval;
	
	xval.reserve(20000);
	yval.reserve(20000);


	cout.precision(8);

	double lol = 0;

		while(true) {      
		if (!file.good() ) break;
		counter++;
		
		string pdline, Stemp;
		getline(file, pdline,',');   
		istringstream istr(pdline.c_str());      
		istr >> Stemp;
		if(counter==1||counter==2)
			continue; 
		if(Stemp == "" )
		break;
		if(counter%2!=0){
			lol=atof(Stemp.c_str());	
			xval.push_back(lol);
			
		}
		else	{
			lol=atof(Stemp.c_str());	
			yval.push_back(lol);	
			}	
		} 
		file.close();

	
		if(tranf_data(xval,yval, fname_base)){
			cout << "data tranf correct" << endl;

			}

	
	return true;
}

bool tranf_data(std::vector<double> &x, std::vector<double> &y, string fname_base){

		cout << "Tranformando data original" << endl;	
		string fname_out = fname_base+".txt";
		cout << "inside final funtion " << fname_out << endl;
		fstream ofile;
		ofile.open(fname_base+".txt", ios::out);
			
		int counter =0,pos=0;
		double max=0;
		bool check=true;		

		for(int j=0; j<x.size(); j++) {
		if(counter>=15 && y.at(j-15)>=1.){
			pos=j-15;
			check=false;
			counter=0;	
			}
			
		if(y.at(j)>max)
			max=y.at(j);
		if(y.at(j)>0 &&	check==true){
			counter++;
			} 
		else if( y.at(j)<=0&&check==true){
			counter=0;
		}			
	//	ofile << x.at(j) << " " << y.at(j) << endl;
		
		}
	
		
		cout << endl << "initial position " << pos << " maximo  " << max << endl << endl;
		cout << "values at initial position " <<  x.at(pos) << "  " <<  y.at(pos) << endl;

		// 
	
	for(int jj=pos; jj<x.size(); jj++) {
		x.at(jj-pos)=x.at(jj)-x.at(pos);
		y.at(jj-pos)=y.at(jj);

	}

	cout << "pos var " << pos << endl;	
	cout << x.size() << " old size" << endl;

		int del=0;

	cout << x.size()-pos << " and " << x.size() << endl;	

	x.erase(x.begin()+(x.size()-pos-1),x.begin()+x.size());
	y.erase(y.begin()+(y.size()-pos-1),y.begin()+y.size());
	
		
	

	cout << "delete spaces " << del <<endl; 
	cout << x.size() << " new size" << endl;

	

	TGraph *g1 = new TGraph(x.size());
	int max_pos = 0;

	for(int k=0; k<x.size(); k++) {
	//	ofile << x.at(k) << " " << y.at(k) << endl;
		g1->SetPoint (k, x.at(k), y.at(k));
		if(y.at(k)==max)
			max_pos=k;
	}


	double x_max_val=0, y_max_val=0;
	g1->GetPoint(max_pos,x_max_val,y_max_val);
	cout << "chequeando max position and values" <<  max_pos  << " " << x_max_val << " " << y_max_val << endl;

	TF1 *mono = new TF1("monoexponential","[0]+[1]*TMath::Exp(-1*x*[2])-[3]*TMath::Exp(-1*x*[4])",0.,10000.);
	mono->SetParameter(0,1.61);
	mono->SetParameter(1,15);
	mono->SetParameter(2,0.02);
	mono->SetParameter(3,100);
	mono->SetParameter(4,0.1);
	mono->SetParLimits(0,0,10000);
//	mono->SetParLimits(1,0,1000);
	mono->SetParLimits(1,0,100);
	mono->SetParName(0,"Y0"); 
	mono->SetParName(1,"A1");
	mono->SetParName(2,"k1");
	mono->SetParName(3,"A2"); 
	mono->SetParName(4,"k2");


	double acota = 0;
	//new grph for plot
	max_pos = 60 ;
	acota = 1000.;
	
	double ncounter = 0;

	for(int l=0; l<x.size()-max_pos; l++) {
	  if(x.at(l+max_pos) > 150. ){
	    ncounter = l;
	    break;
	  }
	}

	cout << endl << " Debuggin ncounter  " <<  ncounter << endl; 		
	
	if(ncounter==0){
		ncounter = x.size()-max_pos;
	}

	TGraph *g2 = new TGraph(ncounter);
	for(int l=0; l<ncounter; l++) {
		g2->SetPoint (l, x.at(l+max_pos), y.at(l+max_pos));
	}
	
	

	TCanvas *c1 = new TCanvas("c1","",800,600) ;
	g1->Draw();
	g1->SetLineColor(kBlue);
	g1->SetMarkerColor(kBlue);
//	c1->SetLogx();
	//c1->SetLogy();
	TString init = fname_base+"_test.pdf";	
	c1->Print(init);
	c1->Print((TString ) fname_base+"test.png");
	c1->SetLogx();
//	g1->Fit(mono,"S","",x_max_val,x.at(x.size()-1));
	g2->Draw("same");
	c1->Print((TString ) fname_base+"selectdata.png");
	g2->Fit(mono,"R");
//	g2->>GetXaxis()->SetUserRange(0.,50.);
	
	
	
//	mono->Draw("same");
	
/*	TF1 *origin =  new TF1("biexponential","[0]+[1]*TMath::Exp(-1*x*[2])-[3]*TMath::Exp(-1*x*[4])",0,1000);
	origin->SetParameter(0,1.61);
	origin->SetParameter(1,156);
	origin->SetParameter(2,0.012);
	origin->SetParameter(3,126.8);
	origin->SetParameter(4,0.03688);
*/	
	
	
	
//	origin->Draw("Same");
	//	
//	c1->SetLogy();
//	c1->SetLogx();
	c1->Print((TString ) fname_base+"fitprint.png");
	delete c1;
	
	cout << "********" << endl;
	cout << "Reciving fit parameters and integrating" << endl;

	double k1=0,A1=0,y_0=0,A2=0,k2=0;
	
	y_0=mono->GetParameter(0);
	A1=mono->GetParameter(1);
	k1=mono->GetParameter(2);
	A2=mono->GetParameter(3);
	k2=mono->GetParameter(4);  
	cout << " Y0 =  " << y_0 << endl; 
	cout << " A1 =  " << A1 << endl; 
	cout << " k1 =  " << k1 << endl; 
	cout << " k2 =  " << k1 << endl; 
	cout << " A2 =  " << A2 << endl; 



	cout << "********" << endl;
	cout << "Calculating Area" << endl;

	//seting intial final pointsa
	double xf=10000, x0=0;
	double_t Area=0;

	Area =  mono->Integral(0,10000);

	cout << "Area " << Area << endl;

	//std::locale l("German_germany");
	ofile.imbue(std::locale(std::cout.getloc(), new DecimalSeparator<char>(',')));
	
	ofile << " Y0\t" << y_0 << endl; 
	ofile << " A1\t" << A1 << endl; 
	ofile << " k1\t" << k1 << endl; 
	ofile << " A2\t" << A2 << endl; 
	ofile << " k2\t" << k2 << endl; 	
	ofile << "Area\t" << Area << endl;
	
	  
	vector<double> roots;
	double s = 10;
	double e = 10000;

	
	FindRoot(roots, mono, s, e,fname_base);
	sort(roots.begin(), roots.end());
      
	
	double int_end = 10000;
	
	if(s!=roots[0]||roots.size()!=0){
	int_end =  ( roots[0] > int_end ) ? int_end : roots[0];
	  }
	
	cout << roots[0] << " first root " << endl<< endl<< endl;
	cout << int_end << " integral goes until " << endl<< endl<< endl;
	
	Area =  mono->Integral(0,int_end);
	
	ofile << " integral goes until " << int_end  << endl ;

	ofile << "Area at root\t" << Area << endl;
	cout << "root Area " << Area << endl;

	
	ofile.close();
	return true;
	}




void Solve(vector<double>& roots, int nest, TF1* f, double s, double e)
{
 double r = 0; 
  r = f->GetX(0, s, e);
 cout << "(" << nest << ") start=" << s << " end=" << e << " root=" << r << endl;
 roots.push_back(r);
 
 /*if(roots.size()>10000);
  return;*//*
 if (r != s && r !=e) {
   Solve(roots, nest+1, f, s, r);
   Solve(roots, nest+1, f, r, e);
 }
 */
 
}


void FindRoot(vector<double>& roots, TF1* f,double s, double e, string fname_base)	{
 // double s = - TMath::PiOver2();
 // double e = 2 * TMath::TwoPi() + TMath::PiOver2();
  cout << endl;
  cout << "start =" << s << endl;
  cout << "end   =" << e << endl;
 // Create the function and wrap it
  	TCanvas *c1 = new TCanvas("c1","",800,600) ;
//	TF1* f = new TF1("Sin Function", "sin(x)", s, e);
  f->Draw();
//  vector<double> roots;
  
  Solve(roots, 0, f, s, e);
  sort(roots.begin(), roots.end());
  double* xx = new double[roots.size()];
  double* yy = new double[roots.size()];
 // If this is put here; then only the first point
 // is shown in the plot.
 //TGraph* gr = new TGraph(7, xx, yy);
  cout << "Found " << roots.size() << " roots:";
  for (int i = 0; i < roots.size(); ++i) {
      xx [i] = roots[i];
      yy [i] = 0;
     cout << xx[i] << " ";
      }
  cout << endl;
  TGraph* gr = new TGraph(7, xx, yy);
  gr->SetMarkerColor(kRed);
  gr->SetMarkerStyle(21);
  gr->Draw("P");
  c1->Update();
 // c1->SetLogx();
  //c1->SetLogy();
  c1->Print((TString ) fname_base+"froot.pdf");
  cout.flush();
}
